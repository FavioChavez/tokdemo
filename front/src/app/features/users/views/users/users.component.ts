import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/core/models';
import { UserService } from 'src/app/core/services/user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  users$: Observable<User[]> = new BehaviorSubject<User[]>([]);

  constructor(readonly userSvc: UserService) {}

  ngOnInit(): void {
    this.users$ = this.userSvc.getUsers().pipe(
      map((user) => {
        return user;
      })
    );
  }
}
