import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserCardComponent } from './components/user-card/user-card.component';
import { UsersComponent } from './views/users/users.component';
import { UsersRoutingModule } from './users-routing.module';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [UserCardComponent, UsersComponent],
  imports: [UsersRoutingModule, CommonModule, MatCardModule],
})
export class UsersModule {}
