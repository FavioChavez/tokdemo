import { Controller, Get } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(readonly userSvc: UsersService) {}

  @Get('')
  get() {
    return this.userSvc.getUsers();
  }
}
