import { Injectable } from '@nestjs/common';
import { of } from 'rxjs';

@Injectable()
export class UsersService {
  getUsers() {
    return of([
      {
        name: 'John',
        lastName: 'Doe',
        age: 22,
        gender: 'male',
      },
      {
        name: 'Juan',
        lastName: 'Perez',
        age: 22,
        gender: 'male',
      },
    ]);
  }
}
