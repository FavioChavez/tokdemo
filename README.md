# TOKDEMO

## Entorno Back

Para levantar el etorno de back utilizar el comando dentro del directorio back

```
npm run start
```

## Entorno front real

Para levantar el etorno de front utilizar el comando dentro del directorio front

```
ng s
```

## Entorno front TOK

Para levantar el etorno de front utilizar el comando dentro del directorio front

```
npm run starttok
```
